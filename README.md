# Tic Tack Toe Backend

## Description

This is an API containing an AI for a tic tac toe game, built in Node.js and Express. The AI was built around the [**Minimax Algorithm**](https://www.neverstopbuilding.com/blog/minimax) and is essentially unbeatable.

## Instructions

The easiest setup is in to install **nodemon** globally: `npm i nodemon -g` and then just run `nodemon` from the root of the application. The application will run on `localhost:3000` by default. Available endpoints are stated in **index.js**

