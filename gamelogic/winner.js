const winningCombo = require('./winningCombo');

// accepts tic-tac-toe board array as a paramenter and determines if the game has a winner.
// Returns 'Tie', 'In Progress', 'X wins', 'O wins' 

let winner = (board) => {

	if (!winningCombo(board) && board.indexOf('') > -1) return 'In Progress';
	if (!winningCombo(board) && board.indexOf('') === -1) return 'Tie';
	if (winningCombo(board)) return board[winningCombo(board)[0]] + ' Wins';

}

module.exports = winner;