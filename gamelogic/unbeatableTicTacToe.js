const move = require('./move');
const addBoard = require('./addBoard');

// accepts tic-tac-toe array as a parameter and returns tic-tac-toe board with optimal next move added to board.
// you never win. ever.

function unbeatableTicTacToe(board){
	let position = move(board);

	return addBoard(board,position);

}

module.exports = unbeatableTicTacToe;