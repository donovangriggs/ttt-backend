// counts the number of moves in a tic-tac-toe array

let gameCounter = (board) => {
	let counter = 0

	board.forEach(element =>{
		if(element !== '') counter++
	});

	return counter;
}

module.exports = gameCounter;