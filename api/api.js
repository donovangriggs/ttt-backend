const unbeatableTicTacToe = require('../gamelogic/unbeatableTicTacToe');
const gameWinner = require('../gamelogic/winner');
const move = require('../gamelogic/move');

let ticTacToe = (req,res) => {
	let input = req.body.board;
	let output = unbeatableTicTacToe(input);
    res.json({board : output});
}

let winner = (req,res) => {
	let input = req.body.board;
	let output = gameWinner(input);
    res.json({winner : output});
}

let nextMove = (req,res) => {
	let input = req.body.board;
	let output = move(input);
    res.json({nextMove : output});
}

module.exports = {
	ticTacToe,
	winner,
	nextMove
};