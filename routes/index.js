var express = require('express');
var router = express.Router();

// /* GET home page. */
// old stuff, nothing to see here....
// router.get('/', function(req, res, next) {
//   // res.render('index', { title: 'Express' });
//   res.json({test: 'ok'})
// });

const api = require('../api/api');

router.post('/',
  api.ticTacToe
);

router.post('/winner',
  api.winner
);

router.post('/move',
  api.nextMove
);


module.exports = router;
